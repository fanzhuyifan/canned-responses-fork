# Canned Responses for triaging Bugzilla tickets
Pre-written text you can copy and paste into Bugzilla tickets in response to various situations.

If you click on the link icon next to each subheader (e.g. for `out-of-support`) then the link to that specific response will be added to your web browser's history and you can easily call it up while triaging bugs without having to leave the browser.

[[_TOC_]]

## Bug reported for Plasma version that's out of support

I'm afraid Plasma XXXX is unfortunately no longer eligible for support or maintenance from KDE.

Plasma is a fast-moving project, and bugs in one version are often fixed in the next one. Please update to Plasma YYYY as soon as your distro offers it to you. If you need support for Plasma XXXX, please contact your distro, who bears the responsibility of providing support for older non-LTS releases.

If this issue is still reproducible in either Plasma YYYY (the current LTS version) or Plasma ZZZZ (the latest released version), feel free to re-open this bug report.

Thanks for understanding!

**Then mark it as RESOLVED UNMAINTAINED**



## Old bug report that was marked as RESOLVED but someone added a new "me too!" comment to it

This is a fairly old bug report and the code has changed a lot since it was reported. There's a very good chance the issue you're experiencing is caused by something else, even if the outward symptoms look and feel the same. Can you please submit a new bug report? Thank you!

**If the commenter re-opened the bug report, close it again with whatever resolution status it had before**



## Old bug report that hasn't been touched in years that might be fixed now

Thank you for the bug report. Unfortunately we were not able to get to it yet. Can we ask you to please check if this is still an issue with either Plasma XXXX (the current LTS version) or Plasma YYYY (the latest released version).

**Then mark it as NEEDSINFO WAITINGFORINFO**



## Bug reported using GitLab issues

Thanks for the bug report! However KDE is not using GitLab issues for bug reporting purposes, so bugs for this project should be submitted at https://bugs.kde.org. Can you re-submit this over there? Thanks!

**Then close it**



## Feature requested using GitLab Issues

Thanks for the feature request! However KDE is not using GitLab issues for feature requests, so they should still be submitted at https://bugs.kde.org and marked with the "Wishlist" severity. Can you re-submit this over there? Thanks!

**Then close it**



## Graphical glitch seen with KWin on Wayland

Thanks for the bug report! Can you attach the output of:
- drm_info (you'll probably need to install it from your package manager)
- qdbus org.kde.KWin /KWin supportInformation

Also please add
> QT_LOGGING_RULES="kwin_*.debug=true"
to your /etc/environment file, reboot, reproduce the problem again, and then immediately upload the
output of:
> journalctl --user-unit plasma-kwin_wayland --boot 0
> sudo dmesg

Thanks!



## Behavioral issue in an app seen on Wayland but not X11

Can you run the app with the WAYLAND_DEBUG=1 environment variable applied? For example, open a terminal window and enter `WAYLAND_DEBUG=1 [command used to launch the app]`

Then reproduce the bug and attach a file with the full output of that command (it will be very long) to this bug report. Thanks!

**Then mark it as NEEDSINFO WAITINGFORINFO**



## Crash in proprietary NVIDIA GPU drivers

The crash backtrace indicates that the problem is in the proprietary NVIDIA drivers.

Please report this issue to the NVIDIA folks, either by sending an email to linux-bugs@nvidia.com or making a post at https://forums.developer.nvidia.com/c/gpu-graphics/linux. It would be helpful to the NVIDIA developers if you could run nvidia-bug-report.sh and attach the resulting file in your report. Thanks!

**Then mark it as RESOLVED UPSTREAM**



## Crash in Mesa AMD/Intel GPU drivers

The crash backtrace indicates that the problem is in the Mesa graphics drivers. Please read https://docs.mesa3d.org/bugs.html and submit a bug report for the Mesa developers. Thanks!

**Then mark it as RESOLVED UPSTREAM**



## Crash in Nouveau NVIDIA GPU drivers

The crash backtrace indicates that the problem is in the Nouveau graphics drivers. Please read https://nouveau.freedesktop.org/Bugs.html and submit a bug report for the Nouveau developers. Thanks!

**Then mark it as RESOLVED UPSTREAM**



## Crash backtrace that doesn't show any KDE code or include debug symbols

Cannot reproduce, no KDE code is implicated in what I can see from the backtrace, and the backtrace has no debug symbols for anything that would help. :(

If this crash is reproducible, could you please install debug symbols, reproduce the crash, and attach a new symbolicated backtrace? See https://community.kde.org/Guidelines_and_HOWTOs/Debugging/How_to_create_useful_crash_reports

Thanks!

**Then mark it as NEEDSINFO BACKTRACE**



## Crash backtrace missing debug symbols

Thank you for the bug report! Unfortunately I can't reproduce the crash myself on current git master, and the backtrace is incomplete and missing debug symbols for the following lines that we need to figure out exactly what's going wrong:

Could you please install debug symbols and attach a new symbolicated backtrace generated by using `coredumpctl gdb` in a terminal window? See https://community.kde.org/Guidelines_and_HOWTOs/Debugging/How_to_create_useful_crash_reports#Retrieving_a_backtrace_using_coredumpctl for details about how to do this.
Thanks again!

**Then mark it as NEEDSINFO BACKTRACE**



## Crash with no backtrace

If something crashed, we need a backtrace of it so we can figure out what's going on. Can you please attach a backtrace of the crash using the `coredumpctl` command-line program, as detailed in https://community.kde.org/Guidelines_and_HOWTOs/Debugging/How_to_create_useful_crash_reports#Retrieving_a_backtrace_using_coredumpctl?

Thanks!

**Then mark it as NEEDSINFO BACKTRACE**



## App or Plasma is hanging/frozen

Can you get a backtrace of what it's doing when it's hung?

1. Run `gdb attach -p $(pidof XXXX_THE_APP_XXXX)` in a terminal window while it's hung
2. If it says "--Type <RET> for more, q to quit, c to continue without paging--" at the bottom of the terminal window, press the "c" key.
3. When you see a prompt that says "(gdb)", type "bt" and press the enter key.
4. If it says "--Type <RET> for more, q to quit, c to continue without paging--" at the bottom of the terminal window, press the "c" key.
5. Copy-and-paste the contents of the terminal window into a comment here.

Thanks!



## Reporter reported multiple distinct issues

Thanks for the bug report! Unfortunately it reports multiple distinct issues, which will make it not actionable. See https://community.kde.org/Get_Involved/Issue_Reporting#Multiple_issues_in_a_single_Bugzilla_ticket for more explanation.

Can I ask you to submit a new bug report for each specific issue? Thanks again!

**Then mark it as RESOLVED NOT A BUG**



## Bug report is un-actionable due to lack of almost all information

Hello! You've reached the KDE bug tracker, which is for tracking and investigating specific bugs. Unfortunately there is not enough information in here to proceed or pinpoint where a bug (if any) in KDE code might be. Can you please read https://community.kde.org/Get_Involved/Issue_Reporting and add more information here, including why you think it might be a bug in any KDE software? If you read through that wiki page and don't feel like it's helped at all, I'd recommend posting a question at https://discuss.kde.org/c/help/6, and hopefully the people there can help you.

Thanks!

**Then mark it as NEEDSINFO WAITINGFORINFO**



## Bug report is unclear

Thanks for the bug report! Unfortunately I'm having a hard time figuring out what bug is being reported here. Can you please read https://community.kde.org/Get_Involved/Issue_Reporting and submit a new bug report that describes a single discrete problem or suggestion? If you read through that wiki page and don't feel like it's helped at all, I'd recommend posting a question at https://discuss.kde.org/c/help/6, and hopefully the people there can help you determine what the problem is and whether it's caused by a bug in any KDE software. If that's determined to be the cause, please don't hesitate to come back here, re-open this bug report, and describe what the problem was determined to be.

Thanks again!

**Then mark it as RESOLVED WAITINGFORINFO**



## Feature request for a giant un-actionable design change

Thanks for sharing this design proposal!

A better place to do this is in the Visual Design Group chatroom, because this isn't really a bug or a discrete feature request (it's more like a bunch of feature requests all mashed together). As such it would make more sense to discuss it in a group setting where existing design choices and constraints that may not be immediately obvious can be explained, and we can go over which elements of this proposal might make sense to integrate.

Check out https://community.kde.org/Get_Involved/Design.

Thanks for understanding!



## Request for development help in a bug report

Hello! You've reached the KDE bug tracker, which is for tracking bugs in KDE software. Unfortunately here on the bug tracker, we cannot offer help with software development topics not clearly related to bugs in KDE software. This includes help setting up a dev environment, compiling specific projects, and so on. You can find appropriate ways to get in contact with the dev team for assistance with your issue at https://community.kde.org/Get_Involved/development#Communicate_with_the_dev_team.

**Then mark it as RESOLVED NOTABUG**



## Issue performing system updates that was clearly caused by distro packaging issues
Sorry this happened. However this is caused by issues in the packaging provided by your distro, not the app used to install the update. You'll need to contact your distro about it.

**Then mark it as RESOLVED DOWNSTREAM**



## Bug reported with Kali Linux

I'm afraid Plasma is not supported on Kali Linux.

Running everything as root is explicitly unsupported and never recommended, and will result in a million and a half other little weirdnesses like this.

If would like support from KDE for issues you encounter, I would encourage you to re-install your system with a more appropriate general-purpose Linux distro, such as one of the ones you can see at https://kde.org/distributions. If the issue still manifests after doing so, feel free to re-open this bug report.

Thanks for understanding!

**Then mark it as RESOLVED UNMAINTAINED**



## kwin_wayland crashed and took down apps with it, but the reporter included a backtrace from an app rather than kwin_wayland

The crash log only tells us that KWin crashed on Wayland. For the time being, it's a known and unavoidable issue that when this happens, all apps crash too. The app crash logs are not useful; all they do is tell us that KWin crashed. But we need to know why KWin crashed. For that, we need a backtrace of the crash for the kwin_wayland process. You may be able to retrieve it with `coredumpctl`. See https://community.kde.org/Guidelines_and_HOWTOs/Debugging/How_to_create_useful_crash_reports#Retrieving_a_backtrace_using_coredumpctl

**Then mark it as RESOLVED BACKTRACE**



## App doesn't get raised and focused when activated on Wayland when it's a non-KDE app, or it was activated by a non-KDE app (If both apps are KDE apps, we can fix the bug)

This happens because the either the activating app or the app being activated does not implement the Wayland-specific activation protocol (xdg_activation_v1). Both need to correctly implement the protocol.

It's the same on X11, but apps have had decades to get this right; on Wayland, the protocol is still rather new and a lot of apps don't yet have support for it. They should. :) So you should report this to the developers of those apps.

**Then mark it as RESOLVED DOWNSTREAM**



## Multi-monitor bug in Plasma (e.g. missing/swapped containments and panels) caused by having an exotic setup before upgrading to Plasma 5.27 that's fixed in a new user account or with fresh config files

Sorry this happened. In Plasma 5.27 we implemented a new system for mapping Plasma desktops and panels to screens that is fundamentally more correct by design, and as a result much more robust. We also added code to migrate old settings to this new system. Unfortunately, due to the non-determinism baked into the old system, the migration code works better the simpler your arrangement of screens, desktops, and panels was. For complex arrangements, we've seen a few reports that sometimes panels or desktops are swapped or missing, as a result of the old settings being in an inconsistent state at the moment of migration. We do have a UI to recover them in the form of the "Manage Desktops and Panels" window, which should let people manually restore their old setup. You can access it like so:

Right-click on desktop > click on "Enter Edit Mode" > a toolbar pops down from the top of the screen > click on "Manage Desktops and Panels"

The good news is that this shouldn't recur going forward due to the new system we have in Plasma 5.27. So hopefully this is the last time it should ever happen!

**Then mark it as RESOLVED WORKSFORME**



## User reports scaling-related issue ultimately found to be caused by them not using the global or per-screen scaling system, and when you gently explain that they're Doing It Wrong, they complain

The issue here is that we in KDE try to be nice and offer customization opportunities so that people can self-satisfy, but often when they do so, the result never looks as good as if the customization wasn't needed because everything worked property out of the box. Additionally, this easy customization reduces the urgency for the 3rd-party apps being worked around to get their houses in order and adopt new technologies.

Scaling in particular is a nightmare because there are no fewer than seven ways to adjust the size of things if you find that they don't look right to you, each with its own subtle drawback and pitfalls:

1. Global or per-screen scaling slider in System Settings
2. Per-screen resolution setting in System Settings
3. Force Font DPI spinbox in System Settings
4. Adjustable font size in System Settings (because lots--but not all--UI controls resize themselves in proportion to the font size)
5. Adjustable icon sizes for certain things in System Settings
6. Adjustable icon sizes in various individual KDE apps (e.g. Dolphin and Gwenview main view, Places Panel sidebar, etc)
7. Whole-app scaling systems in various apps (e.g. scaling slider in Telegram, whole UI responsive to Ctrl+plus and Ctrl+minus in electron apps)

Every time we try to make the overall scaling experience easier to use and better out of the box by getting rid of one of these settings, we get tons of complaints that people were using it to work around such-and-such issue in a 3rd-party app, or in a core technology like XWayland--it's always something that's outside of KDE's control! But when we keep these settings available, it's impossible to ensure that our own software looks right because there are so many ways a user can combine the settings that they can't all possibly be tested for to make sure they don't conflict and produce bad results. So when you use any of these settings except the first one (which is the recommended one), you're kind of on your own.

That was probably too much information, but hopefully it helps to fill in the context a bit. :)

**Then mark it as RESOLVED UNMAINTAINED**



## When using header-color-using color scheme (e.g. Breeze Light or Breeze Dark), User reports that active and inactive titlebar colors in the color scheme editor don't work, or that they can't change the inactive header colors

Breeze Light and Breeze Dark both have Header colors in them. When a color scheme has Header colors in it, its Titlebar active/inactive colors are ignored, and the Header colors are used to create a unified look for the window's entire header area.

This is perhaps not the best design, but until it's changed, it's what we have to live with. There are three options:

1. Switch to a color scheme that does not have Header colors (e.g. the old Breeze color scheme, which is still shipped)
2. Remove the Header colors from your current color scheme
3. Create the desired appearance using the Header colors of your current color scheme, by adjusting the Normal/Inactive background colors in the Header section of the color scheme editor

If you want to use option #2, I'm afraid the procedure is rather annoying since we were have not yet made it possible via the GUI to remove header colors. Sorry about that. To do this manually, you will need to save a copy of the color scheme, open it at ~/.local/share/color-schemes/[the color scheme] and remove the "Header" sections, then switch to another color scheme in the Colors KCM and back to your now-header-less custom color scheme.

**Then mark as a duplicate of either 433059 or 446584, as appropriate**



## User settings and theming aren't respected when running a graphical app with `sudo` or as root

Any settings you change are saved to your user account. But when you run apps using `sudo`, they aren't being run as your user; they're being run as the root user, and the root user account doesn't see customizations made as your normal user.

In general, GUI apps should not be run using `sudo`; they should be smart enough to request elevated privileges themselves when you launch them and use them normally. Any apps that don't do this should be fixed, and ideally not used at all until that happens.

**Then mark it as RESOLVED NOTABUG**



## Autostart app doesn't autostart at all

Can you run `kreadconfig5 --file startkderc --group General --key systemdBoot` in a terminal window and see what it says? If it says "true" or returns no result, then try the following:

1. Run `kwriteconfig5 --file startkderc --group General --key systemdBoot false` in a terminal window
2. reboot

If that fixes the issue, we know it's a problem with the systemd autostart stuff. Then please re-enable systemd boot by deleting the file ~/.config/startkderc, and then reboot again.

After the reboot, please run `/usr/lib/systemd/user-generators/systemd-xdg-autostart-generator` in a terminal and and paste the text that appears, if anything.

Thanks!

**Then mark it as NEEDSINFO WAITINGFORINFO**



## User reports that Wayland app doesn't remember its window position, observes that it works on X11 for them, and requests an explanation of why there's a difference

The TL;DR version is that this will become a reality once all windows are native Wayland windows and Bug 15329 is implemented in KWin.

If you'd like more details, read on. The way window placement works is as follows:

On X11:
Per the X11 spec, windows are  allowed to place themselves on the screen--but they aren't required to. If a window doesn't, then KWin places it according to its placement policy, which is user-controllable. This includes XWayland apps run in a Wayland session.

KWin *could* in theory ignore this and forcibly place X11 windows according to its placement policy. But doing so unconditionally would not only break the spec, but in practice it would also break apps that were developed with the expectation of being able to place their own windows wherever they want, so instead this is an opt-in thing that you as the user have to turn on via the Window Rules system.

Because that would be annoying, many X11 apps implement their own "remember my window positions" feature. And many--but not all--KDE apps do so on X11. In QtWidgets apps that use KDE's KXMLGui framework for the main window, it's a standard feature that apps' main windows get automatically, and whether or not to do it is controlled by the "Allow apps to remember the positions of their own windows, if they support it" checkbox that's on the "Advanced" page of the Window Behavior KCM.

For Kirigami-using apps, there is no central automatic thing for it as with KXMLGui because Kirigami is a tier 1 framework and therefore cannot depend on KConfig, and it would need to do so to be able to write the necessary data into the app's config file. As a result, Kirigami-based apps often implement this logic themselves in a custom way. A way to improve the status quo would be to write a standard "WindowStateSaver" object (or whatever) that lived in like Kirigami-addons or something and contained the window state saving  logic in a central place, and then Kirigami apps that want to use it could opt into it by importing kirigami-addons and adding that object to their windows as needed.

So yes, as you can see, this is a mess. :) On Wayland it's much simpler:

On Wayland:
Native Wayland windows are not allowed to determine where to place themselves on screen at all; the compositor does this. That means the compositor could implement a "remember window positions" feature and it would work automatically for all native Wayland app windows. This is tracked in Bug 15329. Until that happens, the compositor instead simply places all windows according to the placement policy.

**Then mark it as a duplicate of 15329**



## User asks how to upgrade their software after being told they're using something too old

For software installed from your distro's software repositories, the version provided to you is the one they feel comfortable shipping. If you feel that this version is too old, there are a few options:
1. Get a newer version of the software from Flatpak or Snap. This is the best solution for apps, but it not applicable for Plasma itself.
2. Use a PPA/COPR/OBS repo etc to overlay a newer version packaged from somebody else on top of your distro-provided version. This is not recommended as it tends to cause problems updating the system later.
3. Compile a newer version of the software yourself; see https://community.kde.org/Get_Involved/development. This is an advanced option that's not recommended for people who aren't software developers or technical experts.
1. Switch to a different distro and offers software updates on a faster schedule. This is often painful but generally the correct solution if you discover that the distro you chose doesn't have a software update schedule that matches your preferences.



## Discover fails to do something using its PackageKit backend

Does the same thing happen if you use `pkcon` in a terminal window? If so, the problem is in the PackageKit library itself (which Discover and PKcon both use), so I'd encourage you to report the issue at https://github.com/PackageKit/PackageKit/issues. Thanks!


**Then mark it as NEEDSINFO WAITINGFORINFO**



## Discover failed with a bad error message after doing something using its PackageKit backend

Unfortunately this error message comes from the underlying package management system, not Discover. Discover can pass it on to you, but it can't add more information or context where none exists. The problem ultimately comes from the PackageKit plugin provided by whatever distro you're using (or its parent distro, if you're using a distro that's built on top of another one). I wish distros took better care of their PackageKit plugins such that they worked better in general and provided better error messages when they don't, but regrettably many distros do not.

**Then mark it as RESOLVED UPSTREAM**



## Discover failed with an error message about invalid signatures or keys or something like that

Unfortunately this issue is a caused by a design flaw in the PackageKit library that Discover relies upon for this functionality: it provides no way to bubble up interaction requests so that Discover can show them in the GUI. See https://github.com/PackageKit/PackageKit/issues/604.

Equally unfortunately, there is nothing we can do in Discover short of migrating away from that library (which would entail migrating to use another library that does not current exist, and in the process rewriting half the app) until and unless the issue is fixed upstream.

The only workarounds at this point in time are for packagers to not add such interactivity in their packages themselves, or for users to not use them.

**Then mark it as RESOLVED UPSTREAM**
